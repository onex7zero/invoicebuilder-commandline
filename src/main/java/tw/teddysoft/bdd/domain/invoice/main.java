package tw.teddysoft.bdd.domain.invoice; /**
 * Created by user on 2017/3/29.
 */

public class main {
    public static void main(String args[])
    {
        if (args.length == 0)
        {
            System.out.println("尚未輸入參數，請重新執行");
            System.exit(1);
        }

        int taxIncludePrice = 0;
        int taxExcludePrice = 0;
        double VATRate = 0.0;
        int i = 0;
        String message = "輸入參考: main -i 100 -r 0.05 或 main -r 0.05 -e 101";
        while(i!=args.length) {
            switch (args[i]) {
                case ("-i"):
                    taxIncludePrice = Integer.parseInt(args[i+1]);
                    break;
                case ("-r"):
                    VATRate = Double.parseDouble(args[i+1]);
                    break;
                case ("-e"):
                    taxExcludePrice = Integer.parseInt(args[i+1]);
                    break;
                case ("-h"):
                    System.out.println(message);
                    System.exit(1);
                default:
                    System.out.println("無法處理本次輸入QQ喔，請重新開始");
                    System.exit(1);
            }
            i += 2;
        }
        Invoice invoice = InvoiceBuilder.newInstance().
                withVatRate(VATRate).
                withTaxIncludedPrice(taxIncludePrice).
                withTaxExcludedPrice(taxExcludePrice).
                issue();
        System.out.println("計算結果如下:");
        System.out.println("含稅價格:"+invoice.getTaxIncludedPrice());
        System.out.println("未稅價格:"+invoice.getTaxExcludedPrice());
        System.out.println("稅率:"+invoice.getVatRate());
    }
}
